function [state_estimate,P] = kalman_prediction(state_estimate,Ez,Ex,P,a,A,B,C)
% This function performs a kalman prediction step for the man overboard
% tracking algorithm. It uses the variables returned by the
% initialize_kalman function and outputs the predicted state and predicted
% error.


 % Predict state
    state_estimate = A*state_estimate + B*a;

    %Predict covariance
    P = A * P * A' + Ex;

end

