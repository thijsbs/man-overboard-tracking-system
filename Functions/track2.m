function [x,y,corval] = track2(SA,template,binthres,ctresh)
%This function detects the buoy object using template matching.
% It is intended to be used by the man overboard tracking algorithm
% Output
% - x : x location of buoy
% - y : y location of buoy
% - corval : maximum correlation value
% Input
% - SA : The source image search area
% - template : Template image of the object
% - binthres: Threshold value for intensity thresholding
% - ctresh: Threshold value for correlation thresholding

% Defining other static variables
uthres = 0.9;
dilsize = 8;

bw=rgb2gray(SA); %create bw image

%create binary image
binaryImage = (bw > binthres) & (bw < uthres)  ;
binaryImage = imfill(binaryImage, 'holes');

%dilate binary image
dil = strel('disk', dilsize);
binaryImage = imdilate(binaryImage,dil);

% Perform cross-correlation for the RGB values seperately and summing them
c = normxcorr2(template(:,:,3), SA(:,:,3).*binaryImage);
c = c+normxcorr2(template(:,:,2), SA(:,:,2).*binaryImage);
c = c+normxcorr2(template(:,:,1), SA(:,:,1).*binaryImage);
c = c./3;   % Division by 3 to get the average correlation from the 3 channels

corval = max(c(:));
if corval>ctresh                % If the max correlation is higher then threshold,
    [~,ind] = max(c(:));        % give the location as output
    [y,x] = ind2sub(size(c),ind);
    y = y-round(size(template,1)/2);    % Correct for template size
    x = x-round(size(template,2)/2);
else
    y = [];
    x = [];
end