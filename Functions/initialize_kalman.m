function [ state_estimate,Ez,Ex,P,a,A,B,C ] = initialize_kalman(initial_pos,a,dt,measurement_noise,prediction_noise)
% This function initializes the Kalman filter used in the man overboard tracking
% algorithm. The input recquires the initial object position, acceleration
% constant (a), time constant (dt), measurement noise separated in x and y
% components, and the model noise.


state_estimate = [initial_pos(1);initial_pos(2);0;0]; % Initialize state (position first frame, 0 velocity)

Ez = [measurement_noise(1) 0; 0 measurement_noise(2)]; % Measurement covariance matrix
Ex = [dt^4/4 0 dt^3/2 0;...     %state covariance matrix
      0 dt^4/4 0 dt^3/2;...
      dt^3/2 0 dt^2 0;...
      0 dt^3/2 0 dt^2].*prediction_noise; 
P = Ex;                         % Predicted state covariance

% Defining update matrices:
A = [1 0 dt 0;...
     0 1 0 dt;...
     0 0 1 0;...
     0 0 0 1];
 
 B = [dt^2/2; dt^2/2; dt; dt];
 
 C = [1 0 0 0; 0 1 0 0];

end

