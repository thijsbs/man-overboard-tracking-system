function [dis,psizes]= distance(img,x,y,type,focal,average_size,psizes)
% Finding object (buoy) distance from camera using earth spherical model
% dis = distance(img,x,y,type,flength)
% Output:
% - dis = distance from camera to object (earth arc length) in meters

% Input:
% -img : image input
% - x : x-position of object (buoy)
% - y : y-position of object (buoy)
% - type : buoy colour -> 'y' for yellow/gele
% 'o' for orange/oranje
% 'w' for white/witte
% - focal : focal lentgh -> 20 for 20mm lens
% 50 for 50mm lens
% - average_size : Amount of previous size measurements to average
% - psizes : array of previous size measurements with size(average_size)


H = 2.5; %camera height in meters
R = 6371 * 10^3; %earth radius in meters
band = 8;

switch type %height of buoy in milimeters
    case 'y'
        bwidth = 12.6 * 10;
    case 'o'
        bwidth = 34 * 10;
    case 'w'
        bwidth = 51 * 10;
end

load('cameraParams.mat');

switch focal %pixels per-mm
    case 20
        flength = cameraParams20.FocalLength ;
        tes = 20;
    case 50
        flength = cameraParams50.FocalLength;
        tes = 50;
end



%create binary image
img = rgb2gray(img);
imbin = (img > 0.5);

% create area of interest around the detected object
imbin = imbin(y-band:y+band,x-band:x+band);


%create blob detection
props = regionprops(imbin,'BoundingBox');
if ~isempty(props)
    bbox = props.BoundingBox;
    bimwidth = bbox(4);         % The width of the region
else
    bimwidth=[];
end

psizes = [bimwidth psizes];     % Add size to the list

if length(psizes)>average_size
    psizes(end) = [];             % Remove last element from the list
end

bimwidth = mean(psizes);        % Calculate mean of the sizes
standard_dev = std(psizes);     % Calculate standard deviation of sizes
range = [bimwidth+standard_dev bimwidth-standard_dev]; % Express distance as a range

%calculate object to camera distance in meters - note divided by 1000
camdis = flength(2)*bwidth./(range*1000);

%calculate the true distance - arc of earth spherical model
theta = acos(((R+H)^2+R^2-camdis.^2)./(2*(R+H)*R)); %angle of object to camera in radians
dis = theta.*R;


