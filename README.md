# Man overboard tracking system

_Alkindi Rizky and Thijs Bruins Slot_

# Introduction

Finding a drowning person at sea during a rescue attempt can be a difficult task. The constant movement of the waves, the ship and the target, as well as a very small portion of the person being visible above the waterline, can make looking for castaways as difficult as searching for a needle in a haystack. Luckily, in the age of information technology, a tool might be developed using computer vision to aid rescuers in localizing their target. Such a tool could indicate the castaway&#39;s approximate position, even when it might not be apparent to the naked eye. This report provides an attempt at formulating a method and a working algorithm to perform this task.

# Problem definition

Tracking a person at sea has several aspects that need to be taken into account in when formulating this problem. The following sub-problems have been identified:

1. Separating the object of interest from the background (the sea). Only a small portion of the person would be above the water level, therefore it might be difficult to discern the target from the background, especially at a distance.
2. Occlusion of the target by wave motions. The target will periodically be occluded by waves passing in front of it. The tracker algorithm would therefore have no information about the targets location for brief periods of time and should be able to deal with that.
3. Camera motion. Because of the motion of the boat and the shaking of a handheld camera by the cameraman, the video information is highly unstable. An unstable camera adds a large amount of error or deviation to the location estimation. This can make the tracking algorithm inaccurate, therefore a method for stabilizing the video would make tracking more reliable.
4. Distance estimation. The estimated distance to the target might be of interest in a rescue operation.

# Methods

## Background

The algorithm we propose is based on various well-known theoretical concepts. These concepts will be included in this report as background information.

#### Template matching

Object tracking can be achieved through various methods. One such method is the so-called template matching approach, where a target image template is compared to the target image to find the best match. Finding a template match in an image can be achieved using a normalized cross correlation function, which is described by: [1,2]

![enter image description here](https://gitlab.com/thijsbs/man-overboard-tracking-system/raw/master/images/template_matching.png)

Where  is the target image and t the template image.  and are the mean intensity of the image and the template respectively. This equation returns the correlation coefficient , containing values between 0 and 1, with higher values in regions of the image that exhibit higher similarity to the template.

A well-known and widely used method for improving tracking algorithms is the Kalman filter. [3,4]
The Kalman filter is very robust when it comes to measurement noise and temporary signal loss, both of which are a major issue in this application. The detection method as described above is expected to contain noise because of the difficulty to separate the target from the background, and similarly looking waves that can result in false detections. Also occlusion of the target by the waves makes the detection of the target temporarily impossible. The Kalman filter is the best option to counteract these problems and still give a reliable position estimate.

The Kalman filter uses a model of the situation that is uses to make a prediction of the state. In terms of two-dimensional position, as is the case for this problem, the model used to make these predictions is comprised of equations of motion separated in x and y direction:

![enter image description here](https://gitlab.com/thijsbs/man-overboard-tracking-system/raw/master/images/motion_equation.png)

The prediction model for the Kalman filter is implemented in matrix form in the following way:

![enter image description here](https://gitlab.com/thijsbs/man-overboard-tracking-system/raw/master/images/kalman1.png)

The Kalman filter adjusts the prediction model according to the measurements (if available), taking into account both the noise of the measurement and the noise of the prediction. Based on the noise, the prediction and the measurement are weighted to arrive at a position estimate. The measurement by the template matching only measures the position. For use in the Kalman filter the measurement in relation to the prediction is defined as:

![enter image description here](https://gitlab.com/thijsbs/man-overboard-tracking-system/raw/master/images/kalman2.png)

The error associated with the measurement  and the time dependent error associated with the prediction model , are defined as follows:

![enter image description here](https://gitlab.com/thijsbs/man-overboard-tracking-system/raw/master/images/kalman_error.png)

The Kalman filter makes a prediction step based on the previous model where it makes a prediction of the state (position and velocity) and of the prediction error (P):

![enter image description here](https://gitlab.com/thijsbs/man-overboard-tracking-system/raw/master/images/kalman_prediction.png)

If a measurement is available, the Kalman filter will then update the predicted state and prediction error with the measurement, taking into account the weighting factor (K) based on the measurement and the prediction noise:

![enter image description here](https://gitlab.com/thijsbs/man-overboard-tracking-system/raw/master/images/kalman_update.png)

#### Edge detection

In a rescue operation at sea, the only reference that could be used for stabilizing the video is the horizon. The horizon gives a clear contrast between the sky and the sea and can therefore easily be detected using edge detection. The Canny edge detection algorithm is an efficient and accurate method for detecting edges. The Canny edge detection applies a Gaussian filter to blur the image and finds the gradients. In the case of the horizon detection only the vertical gradient is of interest. The second order derivative is obtained by convolving the image with the second order derivative of the Gaussian kernel. The edge is localized by taking the zero-crossings of the second order derivative. This results in a map of potential edges of varying edge strengths, with the strongest edge being the horizon, as long as the horizon can be assumed to be clear of any structures. [5]

#### Prospective projection

An image projected on the CCD chip of a camera is two-dimensional. This image is a result of perspective projection of real three-dimensional scene. How the projection applies in this case is modeled by a so-called pin-hole camera model that is illustrated in figure below:

![enter image description here](https://gitlab.com/thijsbs/man-overboard-tracking-system/raw/master/images/prospective_projection.png)

The image plane in the figure above is reflected with respect to the XY to show a non-mirrored image illustration. The camera lens is treated as a hole for projecting the image, which is located in the base of z-axis (z = 0). How the x, y, and z coordinates of a point 3D space are projected on the camera screen depends on the focal distance , the distance between the pinhole and the screen. The  coordinates on the camera screen can with this model be related to the object distance  with the following relationship:

This allows one to estimate the distance of the object to a well calibrated camera if the focal distance  and both the  and coordinates of the object are known. [6]

![enter image description here](https://gitlab.com/thijsbs/man-overboard-tracking-system/raw/master/images/earth_spherical_model.png)

The camera is not located at the same height as object and the earth surface curves around, as can be seen in the figure above. This phenomenon starts to play a role at great distances, as would be expected to be the case in a rescue operation at sea. The camera is placed at a known height (h) above sea surface. The distance (D) is not true distance in the same sea level. The true distance is the length of arc segment x in the figure. Using trigonometry and the known earth radius in the following equation, the true distance x can be calculated:

![enter image description here](https://gitlab.com/thijsbs/man-overboard-tracking-system/raw/master/images/distance_eq.png)

## Strategy

#### Horizon stabilization

In computer vision, video stabilization is often done by using feature tracking of corner points. This requires stationary objects with detectable features to be present throughout the video. Out at sea however, there are no such structures and everything in the video is always changing. The only &quot;structure&quot; that should be present in every frame of the video is the horizon. We propose a method to stabilize the video using only the detection of the horizon. We make the assumption here that at sea, the horizon is completely clear across the entire frame, however it could also work relatively well with objects like trees on the horizon.

We propose to detect the horizon using a vertical edge detection algorithm. Because the horizon is the most prominent edge in the frame, it can be obtained by getting the maximum value for each image column. To save memory and computation time, it would be sufficient to only sample the horizon with a few equally spaced columns out of the image. A line can be fitted through the sampled horizon points. This line can be represented in first order polynomial coefficients corresponding to the offset and the slope of the line. These coefficients can be used to construct an affine transformation matrix to stabilize the video frame.

![enter image description here](https://gitlab.com/thijsbs/man-overboard-tracking-system/raw/master/images/algorithm1.png)

#### Tracking

A template-matching cross-correlation approach is used to distinguish the object of interest from the sea and the waves, as explained above. Because the object is constantly partly occluded by the waves and is therefore changing shape in the movie frames, we explored a method of using a template dictionary. For a real life application a comprehensive dictionary could be defined to create templates of how a castaway would be expected to look like at sea from afar. The person of interest can be considered as a small blob with different color and intensity from the background water, especially from great distances. The diversity of the templates would in that case be within a reasonable finite range of possibilities, allowing for a realistic dictionary size.

The template matching would be far too inefficient and prone to false positive errors when applied to the whole image. A search area around the location of the object&#39;s last known location is therefore desirable. For this we make the assumption that the initial position of the object is known. The video stabilization is done only in y-direction. In the case of rapid camera movement the y-direction will be very stable but the x-direction can still move freely, making it more likely for the object to be lost outside of the search area in the x-direction. Therefore the shape of the search area should wider in the x-direction then the y-direction.

Another assumption that can be made and that can greatly improve the stability of the tracking is the assumption that the distance of the object to the horizon does not change very fast at all, especially at great distances. This makes it possible to reject any false positive positions that changed quickly in y direction with respect to the horizon.

The algorithm for the template matching is defined below (algorithm 2) and the implementation of the template dictionary is defined in algorithm 3.

![enter image description here](https://gitlab.com/thijsbs/man-overboard-tracking-system/raw/master/images/algorithm2.png)

![enter image description here](https://gitlab.com/thijsbs/man-overboard-tracking-system/raw/master/images/algorithm3.png)

#### Using the Kalman Filter

For the implementation of the Kalman filter, the assumption was made that there is no acceleration of the target object in the model (a = 0). The measurement noise ($\sigma_x$, $\sigma_y$) can be assumed to be much higher than the prediction noise due to the constant and sudden camera movements. These movements have partly been stabilized in the y direction, but not in the x direction. Therefore the assumption can be made that $\sigma_x > \sigma_y$.

The initialization algorithm of the Kalman filter is defined as follows:

![enter image description here](https://gitlab.com/thijsbs/man-overboard-tracking-system/raw/master/images/algorithm4_1.png)![enter image description here](https://gitlab.com/thijsbs/man-overboard-tracking-system/raw/master/images/algorithm4_2.png)


Through experimenting with these Kalman filter initialization parameters we found that the most robust behavior of the filter was with the following values:

- a = 0
- df = 1
- $\sigma_xy$ = [150 200]
- $\sigma$ = 0.1 

Each frame the Kalman filter projects the state ahead based on the previous model. Then, for the new frame with the new position prediction, the search area for the template-matching tracking algorithm is updated according to this prediction. If the tracking algorithm detects the object, the Kalman filter updates the prediction and the model. These prediction step and the update step have been divided here into two separate functions.

![enter image description here](https://gitlab.com/thijsbs/man-overboard-tracking-system/raw/master/images/algorithm5.png)

![enter image description here](https://gitlab.com/thijsbs/man-overboard-tracking-system/raw/master/images/algorithm6.png)

#### Distance estimation

The distance estimation is based on perspective projection as described above. It relates the known size of the object with the observed size in the video frame. The actual size of the object is known by direct measurement of the object _a priori_. The needed focal length is known by applying camera calibration, also _a priori_. The distance can then be calculated by measuring the size of the object in the image. This is done by using MATLAB&#39;s regprop command on the binary region of the object in the image. Because the observed size often lies within the range of 1-5 pixels, the distance estimation is quite inaccurate and makes big jumps of hundreds of meters. The assumption can be made that the distance is not changing very fast in the case of a moving boat. Therefore, to make the distance estimation more stable and reliable, the observed size is averaged over a large number of frames (30 or more). This means that initially, the reliability of the distance estimation will increase until sizes from enough frames are collected. It should be noted however that this method is still expected to be very inaccurate. For this reason we decided to let the algorithm return a distance range, rather then a single value. The algorithm for this distance estimation is described below:

![enter image description here](https://gitlab.com/thijsbs/man-overboard-tracking-system/raw/master/images/algorithm7.png)

## Evaluation

#### Experimental setup

Several video files were supplied for this assignment to test the algorithm for a real life situation of a rescue operation. Instead of an actual castaway, a floating buoy was used with varying size and color. Each video is made with different conditions, such as distance to the buoy, wave direction, light direction and buoy size and color. These varying conditions allow for testing the robustness and reliability of the algorithm in several scenarios.



#### Performance assessment

To quantitatively assess the performance of the algorithm, three testing measures are defined that are related to the desired behavior of the algorithm. These measures are:

- Total tracking time (in frames) until the object is truly lost
- Percentage of false positives in tracking
- Percentage of false negatives in tracking

The total tracking time in frames is the amount of frames that the algorithm is able to keep track of the object before a drift occurs that makes the algorithm unable to find the object back. The rate of false positives is determined by the amount of wrong tracking measurements that the template-matching algorithm produces on average during the tracking period. That is to say, the percentage of frames where the tracker detects a point that is not the object of interest. The percentage of false negatives is determined by the number of times the tracker detects nothing whilst the object is visible. The last situation should be less severe as the Kalman filter will still be able to make predictions based on previous measurements, false positives however are more problematic for the system because the Kalman filter adjusts its model based on these wrong positions. Both percentages of false positives and false negatives are assessed by manual inspection of the buoy location and the position measurement.

# Results

The performance of the algorithm has been assessed for each video and is included in table 1. For each video, the three testing measures are shown as well as the main difficulty of the video.

Table 1: Results of the performance assessment by determining the total tracked frames, the false positive measurements and the false negative measurements.

| **Video** | **Total tracking [frames]** | **False positives [% of frames]** | **False negatives [% of frames]** | **Main difficulty** |
| --- | --- | --- | --- | --- |
| MAH01321.MP4 | - | - | - | Sunlight reflection |
| MAH01340.MP4 | 65 | 9 | 29 | Large distance |
| MAH01357.MP4 | 74 | 15 | 22 | Large camera movements |
| MAH01367.MP4 | 251\* | 35 | 11 | Large distance |
| MAH01406.MP4 | 10 | 0 | 40 | Object not visible for too long |
| MAH01433.MP4 | 58 | 14 | 16 | Camera movement / short visibility |
| MAH01462.MP4 | 426\* | 16 | 3 | - |
| \* The algorithm was able to keep track of the object till the end of the video |





# Discussion

We have found this problem to be quite a challenge. The constant changing of the environment, inconsistent camera movements, the tiny, mostly occluded target in the distance, often indistinguishable from waves make the process of tracking this object quite troublesome. With the algorithm proposed here, we have found a somewhat reasonable performance, given these challenges. However the performance is nowhere near where we hoped it would be. Probably the biggest difficulty we have encountered is the large similarity between the target and the surrounding waves. The false positives in position measurement this produces is particularly problematic when the actual object has been occluded for a substantial period of time. The Kalman filter will in that case weigh those false measurements heavier because of the growing prediction error in prolonged absence of measurements. This causes the Kalman prediction model to drift away from the actual location, causing the algorithm to lose the object often before it can reemerge to be detected again. We have tried many ideas to combat this challenge, the simplest that is implemented here being to use a higher correlation threshold to limit the false positives and consequently produce more false negatives, which are less detrimental. We experimented also with a search area that slowly grows in x-direction in the absence of measurements. This allows the target to stay in the search area longer when a drift occurs for the algorithm to detect it once it reemerges. This seems to work quite well in most cases, but has an adverse effect in other cases. The most ideal method to reduce the false positives is to differentiate between the target and the waves through methods other then visual appearance alone. The waves can be assumed to behave differently over time then the target with different velocity and direction. We tried differentiating the target and the waves in this way but unfortunately without success. Optical flow and feature tracking is not an option in this situation because the properties of the waves change too rapidly to track them. We experimented with subtracting binary frames sequentially and attempting to estimate the direction that way. While this first step worked marginally well, estimating and comparing the direction of the target appeared to be very unreliable and the whole procedure is too cumbersome to be of any practical use. The template dictionary implementation seems to somewhat negate the effect of false positives by providing multiple measurements per frame, making it less likely that the false positive is the only measurement to rely upon. Another major problem is the camera movements in x-direction. This is especially problematic when the object is occluded at the time of the movement, preventing any feedback of the movement to the Kalman filter. The only way around this problem is to have a reference object in view of the camera.

As shown in table 1, the algorithm was able to keep track of the object throughout the entire duration of the video in two cases, however in one case this was partly due to luck with some false positive measurements pulling the Kalman filter in the right direction. All the other video&#39;s lost the object sooner or later. In the case of the video with the sunlight reflection we were not even able with great effort to distinguish the object from the reflections ourselves and decided that tracking in such a situation is not realistic. We noticed that the Kalman filter was very useful for making the process robust in most cases. However especially with more rapid and extreme camera movements, the prediction model error should not be too low to be able to follow this movement, but this results in a greater drift when false positives are detected.

The current implementation of the algorithm in MATLAB is very slow with frame rates ranging from 0.5 to 1 fps. It should be noted though that even playing the HD video in MATLAB without any other computation is only marginally faster. Therefore we expect that if this algorithm were to be implemented with better utilization of native video streaming capabilities, or even in dedicated hardware, the playback time may be dramatically improved. We expect that aside from MATLAB&#39;s video playback limitations, applying the affine transformation to HD frames for video stabilization is most computationally intensive part of the algorithm.

Because we were generally not satisfied with how the algorithm operated and were determined to try to make work as best as possible, the project suffered a substantial delay. Despite of this delay however, the algorithm is still not functioning as optimally as we would like it to be. This has at least come to a reasonable performance, and by now we should accept that perfection is not always a possibility.

# Conclusion

The presented algorithm is able to accurately keep track of an object at sea, however there are several limitations to this approach that make it impractical for real life situations. Errors arise mainly from camera movements and bad visibility of the target. For this to work properly, the object of interest should be more clearly visible, possibly less far in the distance. Also, efforts should be made to make the camera as stable as possible for best results. In case of a rescue operation it would greatly help if the castaway were equipped with a clearly visible beacon, either illuminated or brightly colored. Also the camera would be best mounted on deck with active movement stabilization. With these adjustments, we believe that the presented algorithm can provide a helpful tool in rescue operations.

# References

[1] K. Briechle, U. D. H. Template Matching using Fast Normalized Cross Correlation. _Proceedings of SPIE_ **4387** , 95-102 (2001).

[2] Lewis, J. P. Fast Normalized Cross-Correlation. _Vision Interface_, 120-123 (1995).

[3] G. Welch, G. B. An Introduction to the Kalman Filter. _Dep. Computer Science, University North Carolina_ (1995).

[4] Kalman, R. E. A New Approach to Linear Filtering and Prediction Problems. _J. Basic Eng_ **82** , 35-45 (1960).

[5] M. Sonka, V. H., R. Boyle. in _Image Processing, Analysis, and Machine Vision_    Ch. 5, 116-147 (Cengage Learning, 2007).

[6] M. Sonka, V. H., R. Boyle. in _Image Processing, Analysis, and Machine Vision_    Ch. 2, 11-14 (Cengage Learning, 2007).

.